require("postcss-preset-env");
require("tailwindcss");
module.exports = {
  plugins: ["tailwindcss", "postcss-preset-env"],
};
